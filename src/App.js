import logo from './logo.svg';
import './App.css';
import { Button } from './components/Button';
import { CopyText } from './components/CopyText';
import { useState } from 'react';
import { PokemonList } from './components/PokemonList';

function App() {
  const [theme, setTheme] = useState()

  return (
    <div className="App">
      <Button theme={theme} setTheme={setTheme} />
      {
        theme === "dark" &&
        <CopyText theme={theme} />
      }
      <PokemonList />
    </div>
  );
}

export default App;
