import "./Button.css"

export const Button = ({ theme, setTheme }) => {
    const handleClick = (e) => {
        if (theme === "dark"){
            setTheme("")
        } else {
            setTheme("dark")
        }
    }

    return (
        <button 
            onClick={handleClick}
            className={theme === "dark" ?
            "button-dark" :
            "button"
        }>Click me</button>
    )
}