import { useEffect, useState } from "react"

export const PokemonList = () => {
    const [pokemon, setPokemon] = useState([])

    useEffect(() => {
        getPokemon()
    }, [])
    
    const getPokemon = async () => {
        
        const data = await fetch("https://pokeapi.co/api/v2/pokemon/")
        const parsedData = await data.json()
        console.log(parsedData)
        setPokemon(parsedData.results)
    }
    

    return <>
        <h1>Pokemon List</h1>
        <ul>
            {pokemon.map((element, i) => {
                return <div>
                        <li key={i}>
                            <a href={element.url}>{element.name}</a>
                        </li>
                    </div>
            })}
        </ul>
    </>
}